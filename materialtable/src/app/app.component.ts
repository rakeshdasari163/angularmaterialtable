import { AfterViewInit, Component, ElementRef, Inject, OnInit, ViewChild } from '@angular/core';
import { Router } from '@angular/router';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { MatTableDataSource } from '@angular/material/table';
import { MatDialog } from '@angular/material';
//import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
//import { ReadMessageComponent } from '../read-message/read-message.component';
export interface Student {

  id: number;
  name: string;
  address: string;
  position: string;
}


@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})

export class AppComponent implements OnInit{

  title = 'materialtable';students: Student[] = [
    { id: 1, name: "abc", address: "mumbai", position: "accountant" },

    { id: 2, name: "pqr", address: "mumbai", position: "developer" },

    { id: 3, name: "xyz", address: "mumbai" ,position: "jr" },

    { id: 4, name: "aaa", address: "mumbai", position: "sr" },

    { id: 5, name: "bbb", address: "mumbai", position: "accountant" },

    { id: 6, name: "ccc", address: "mumbai", position: "developer" },
  ]

  displayedColumns: string[] = ['id', 'name', 'address', 'position'];
  dataSource = new MatTableDataSource<Student>(this.students);

  @ViewChild(MatPaginator,{static: false}) paginator: MatPaginator;
  @ViewChild(MatSort,{static: false}) sort: MatSort;

  constructor(private router: Router, private dialog: MatDialog) { }

  ngOnInit(): void {
    console.log(this.students);
  }
  ngAfterViewInit() {
    this.dataSource.paginator = this.paginator;
    this.dataSource.sort = this.sort;
  }
  applyFilter(event: Event) {
    const filterValue = (event.target as HTMLInputElement).value;
    this.dataSource.filter = filterValue.trim().toLowerCase();

    if (this.dataSource.paginator) {
      this.dataSource.paginator.firstPage();
    }
  }
}
  

function ReadMessageComponent(ReadMessageComponent: any, arg1: { data: any; }) {
  throw new Error('Function not implemented.');
}

